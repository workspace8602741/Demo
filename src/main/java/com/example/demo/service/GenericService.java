package com.example.demo.service;

import com.example.demo.dto.AccountDto;
import com.example.demo.dto.TransactionDto;
import com.example.demo.dto.UserDto;
import com.example.demo.exception.exceptions.DataException;
import com.example.demo.model.AccountRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.UUID;

import static com.example.demo.service.PersistenceService.EXTERNAL_ACCOUNT;

@Service
public class GenericService {

    @Autowired
    PersistenceService persistenceService;

    @Autowired
    TransactionService transactionService;

    public AccountDto createAccount(AccountRequest request) {

        AccountDto dto = request2Dto(request);

        persistenceService.addAccount(dto);

        int compare = request.getInitialCredit().compareTo(BigDecimal.ZERO);
        // check initialCredit is not negative nor zero
        if(compare < 0)
            throw new DataException("Initial credit cannot be negative");
        else if(compare == 0)
            return dto;

        TransactionDto initialTransaction = new TransactionDto();
        initialTransaction.setAccountIdFrom(EXTERNAL_ACCOUNT);
        initialTransaction.setAccountIdTo(dto.getAccountId());
        initialTransaction.setAmount(request.getInitialCredit());

        transactionService.sendTransaction(initialTransaction);

        return dto;
    }

    public UserDto retrieveUser(String customerId) {

        return persistenceService.retrieveUser(customerId);
    }

    private AccountDto request2Dto(AccountRequest request) {

        AccountDto dto = new AccountDto();
        dto.setAccountId(UUID.randomUUID().toString());
        dto.setCredit(BigDecimal.ZERO);
        dto.setCustomerId(request.getCustomerId());

        return dto;
    }
}
