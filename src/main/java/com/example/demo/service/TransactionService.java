package com.example.demo.service;

import com.example.demo.dto.AccountDto;
import com.example.demo.dto.TransactionDto;
import com.example.demo.exception.exceptions.GenericException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.UUID;

@Service
public class TransactionService {

    @Autowired
    PersistenceService persistenceService;

    public void sendTransaction(TransactionDto dto) {

        AccountDto accountFrom = persistenceService.retrieveAccount(dto.getAccountIdFrom());
        AccountDto accountTo = persistenceService.retrieveAccount(dto.getAccountIdTo());

        BigDecimal transactionAmount = dto.getAmount();
        BigDecimal accountFromCredit = accountFrom.getCredit();
        BigDecimal accountToCredit = accountTo.getCredit();

        if(accountFromCredit.subtract(transactionAmount).compareTo(BigDecimal.ZERO) < 0)
            throw new GenericException("Cannot complete transaction due to capacity of the account");

        accountFrom.setCredit(accountFromCredit.subtract(transactionAmount));
        accountTo.setCredit(accountToCredit.add(transactionAmount));

        if(dto.getDate() == null)
            dto.setDate(OffsetDateTime.now());

        if(dto.getTransactionId() == null)
            dto.setTransactionId(UUID.randomUUID().toString());

        persistenceService.addTransaction(dto);
    }
}
