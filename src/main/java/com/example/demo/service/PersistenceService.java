package com.example.demo.service;

import com.example.demo.dto.AccountDto;
import com.example.demo.dto.TransactionDto;
import com.example.demo.dto.UserDto;
import com.example.demo.exception.exceptions.DataException;
import com.example.demo.exception.exceptions.NotFoundException;
import jakarta.annotation.PostConstruct;
import lombok.Data;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Service
@Data
public class PersistenceService {

    public static final String EXTERNAL_CUSTOMER = "EXTERNAL_CUSTOMER";
    public static final String EXTERNAL_ACCOUNT = "EXTERNAL_ACCOUNT";

    private Map<String, UserDto> userMap;

    private Map<String, AccountDto> accountMap;

    private Map<String, TransactionDto> transactionMap;

    @PostConstruct
    private void init() {
        userMap = new HashMap<>();
        accountMap = new HashMap<>();
        transactionMap = new HashMap<>();

        // INSERT GENERIC EXTERNAL USER
        UserDto externalUser = new UserDto();
        externalUser.setCustomerId(EXTERNAL_CUSTOMER);
        externalUser.setBalance(BigDecimal.valueOf(Double.MAX_VALUE));

        addUser(externalUser);

        // INSERT GENERIC EXTERNAL ACCOUNT WITH HUGE AMOUNT
        AccountDto externalAccount = new AccountDto();
        externalAccount.setCustomerId(EXTERNAL_CUSTOMER);
        externalAccount.setCredit(BigDecimal.valueOf(Double.MAX_VALUE));
        externalAccount.setAccountId(EXTERNAL_ACCOUNT);

        addAccount(externalAccount);
    }

    public void addAccount(AccountDto dto) {

        accountMap.put(dto.getAccountId(), dto);
        // add account to current user
        UserDto userDto = retrieveUser(dto.getCustomerId());
        userDto.getAccountList().add(dto);
    }

    public void addTransaction(TransactionDto dto) {

        transactionMap.put(dto.getTransactionId(), dto);
        // add transaction to accounts involved
        AccountDto accountFrom = retrieveAccount(dto.getAccountIdFrom());
        AccountDto accountTo = retrieveAccount(dto.getAccountIdTo());

        accountFrom.getTransactionList().add(dto);
        accountTo.getTransactionList().add(dto);
    }

    public void addUser(UserDto dto) {
        userMap.put(dto.getCustomerId(), dto);
    }

    public UserDto retrieveUser(String customerId) {

        if(customerId == null)
            throw new DataException("CustomerId is not valid");

        UserDto userDto = userMap.get(customerId);

        if(userDto == null)
            throw new NotFoundException("User " + customerId + " not found");

        return userDto;
    }

    public AccountDto retrieveAccount(String accountId) {

        if(accountId == null)
            throw new DataException("AccountId is not valid");

        AccountDto accountDto = accountMap.get(accountId);

        if(accountDto == null)
            throw new NotFoundException("Account " + accountId + " not found");

        return accountDto;
    }

}
