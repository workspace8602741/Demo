package com.example.demo.model;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class AccountRequest {

    @NotNull
    private String customerId;

    @NotNull
    private BigDecimal initialCredit;
}
