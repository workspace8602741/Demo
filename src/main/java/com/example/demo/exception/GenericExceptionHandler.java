package com.example.demo.exception;

import com.example.demo.exception.exceptions.DataException;
import com.example.demo.exception.exceptions.ErrorMessage;
import com.example.demo.exception.exceptions.GenericException;
import com.example.demo.exception.exceptions.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.time.OffsetDateTime;

@ControllerAdvice
public class GenericExceptionHandler {

    @ExceptionHandler(GenericException.class)
    protected ResponseEntity<?> handleGenericException(GenericException ex, WebRequest request) {

        ErrorMessage message = new ErrorMessage();
        if(ex.getMessage() != null)
            message.setDescription(ex.getMessage());
        else
            message.setDescription("Generic error");
        message.setDate(OffsetDateTime.now());

        return new ResponseEntity<>(message, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(NotFoundException.class)
    protected ResponseEntity<?> handleNotFoundException(NotFoundException ex, WebRequest request) {

        ErrorMessage message = new ErrorMessage();
        if(ex.getMessage() != null)
            message.setDescription(ex.getMessage());
        else
            message.setDescription("Resource not found");
        message.setDate(OffsetDateTime.now());

        return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(DataException.class)
    protected ResponseEntity<?> handleDataException(DataException ex, WebRequest request) {

        ErrorMessage message = new ErrorMessage();
        if(ex.getMessage() != null)
            message.setDescription(ex.getMessage());
        else
            message.setDescription("Data not valid");
        message.setDate(OffsetDateTime.now());

        return new ResponseEntity<>(message, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}