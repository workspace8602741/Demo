package com.example.demo.exception.exceptions;

import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class ErrorMessage {

    private String description;

    private OffsetDateTime date;
}
