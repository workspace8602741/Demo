package com.example.demo.exception.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class DataException extends RuntimeException {
    public DataException() {
        super();
    }
    public DataException(String message, Throwable cause) {
        super(message, cause);
    }
    public DataException(String message) {
        super(message);
    }
    public DataException(Throwable cause) {
        super(cause);
    }
}
