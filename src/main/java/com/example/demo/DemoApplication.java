package com.example.demo;

import com.example.demo.dto.AccountDto;
import com.example.demo.dto.TransactionDto;
import com.example.demo.dto.UserDto;
import com.example.demo.model.AccountRequest;
import com.example.demo.service.GenericService;
import com.example.demo.service.PersistenceService;
import com.example.demo.service.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import java.math.BigDecimal;
import java.util.UUID;

@SpringBootApplication
public class DemoApplication {

	private static Logger logger = LoggerFactory.getLogger(DemoApplication.class);

	@Autowired
	GenericService genericService;

	@Autowired
	PersistenceService persistenceService;

	@Autowired
	TransactionService transactionService;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@EventListener(ApplicationReadyEvent.class)
	public void exampleInit() {

		// create 2 users
		UserDto firstUser = new UserDto();
		firstUser.setCustomerId(UUID.randomUUID().toString());
		firstUser.setBalance(BigDecimal.ZERO);
		persistenceService.addUser(firstUser);

		UserDto secondUser = new UserDto();
		secondUser.setCustomerId(UUID.randomUUID().toString());
		secondUser.setBalance(BigDecimal.ZERO);
		persistenceService.addUser(secondUser);

		// add one accounts to each user
		AccountRequest firstAccReq = new AccountRequest();
		firstAccReq.setCustomerId(firstUser.getCustomerId());
		firstAccReq.setInitialCredit(BigDecimal.valueOf(100L));
		AccountDto firstAccount = genericService.createAccount(firstAccReq);

		AccountRequest secondAccReq = new AccountRequest();
		secondAccReq.setCustomerId(secondUser.getCustomerId());
		secondAccReq.setInitialCredit(BigDecimal.valueOf(200L));
		AccountDto secondAccount = genericService.createAccount(secondAccReq);

		// perform a transaction between first and second account of 50
		TransactionDto transactionDto = new TransactionDto();
		transactionDto.setAccountIdFrom(firstAccount.getAccountId());
		transactionDto.setAccountIdTo(secondAccount.getAccountId());
		transactionDto.setAmount(BigDecimal.valueOf(50L));
		transactionService.sendTransaction(transactionDto);

		// log both user ids
		logger.info("UserId of first user: " + firstUser.getCustomerId());
		logger.info("UserId of second user: " + secondUser.getCustomerId());
	}
}
