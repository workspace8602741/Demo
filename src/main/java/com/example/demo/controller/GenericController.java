package com.example.demo.controller;

import com.example.demo.dto.AccountDto;
import com.example.demo.dto.UserDto;
import com.example.demo.model.AccountRequest;
import com.example.demo.service.GenericService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class GenericController {

    @Autowired
    GenericService genericService;

    @RequestMapping(value = "/account", method = RequestMethod.POST)
    public ResponseEntity<AccountDto> createAccount(@Valid @RequestBody(required = true)
                                                        AccountRequest request) {

        AccountDto account = genericService.createAccount(request);

        return new ResponseEntity<>(account, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/user/{customerId}", method = RequestMethod.GET)
    public ResponseEntity<UserDto> createAccount(@PathVariable("customerId") String customerId) {

        UserDto user = genericService.retrieveUser(customerId);

        return new ResponseEntity<>(user, HttpStatus.OK);
    }


}
