package com.example.demo.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
public class UserDto {

    private String customerId;

    private String name;

    private String surname;

    private BigDecimal balance;

    private List<AccountDto> accountList = new ArrayList<>();

    public BigDecimal getTotalBalance() {

        BigDecimal totalAmount = BigDecimal.ZERO;
        for(AccountDto dto : accountList)
            totalAmount = totalAmount.add(dto.getCredit());

        return totalAmount;
    }
}
