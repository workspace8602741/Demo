package com.example.demo.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

@Data
public class TransactionDto {

    private String transactionId;

    private String accountIdFrom;

    private String accountIdTo;

    private BigDecimal amount;

    private OffsetDateTime date;
}
