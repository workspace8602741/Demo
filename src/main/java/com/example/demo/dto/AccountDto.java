package com.example.demo.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
public class AccountDto {

    private String customerId;

    private BigDecimal credit;

    private String accountId;

    private List<TransactionDto> transactionList = new ArrayList<>();
}
